/*
Homework #2
A game where the player creates a character and levels it up to kill a dragon.

Refinement #4f - Makin' monsters.
*/

import java.util.Random;
import java.util.Scanner;
public class DragonSlayer4f

{
    private static Random randomGenerator = new Random();
    private enum Status {CONTINUE, WIN, LOSE, QUIT};
    public static Scanner input = new Scanner(System.in);
    private static Status gameStatus;
    public static int userSelection;
    public static final int ADVENTURE = 1;
    public static final int DRAGON = 2;
    public static final int QUIT = 3;
    public static int heroLevel =1;
    private static int heroHealth = 0;
    private static int heroAttackPower = 0;
    private static int heroMagicPower = 0;
    private static final int BUY_HP = 1;
    private static final int BUY_AP = 2;
    private static final int BUY_MP = 3;
    public static final int GOBLIN = 0;
    public static final int ORK = 1;
    public static final int TROLL = 2;
    String monster;
    public static int monsterHealth;
    public static int monsterAttackPower;
    public static int monsterExperiencePoint;
    public static boolean isFighting;
    public static void main(String[] arvs)
    
    { //starts main 
    
    System.out.println("Your village is being attacked by monsterous creatures. Kill a dragon to become a hero and win.");
    createCharacter();
    
        gameStatus = Status.CONTINUE;
        while (gameStatus == Status.CONTINUE)
        {
        printMainMenuPrompt();
        exectueAdventureChoice();
        }
        printEndMessage();
    }//ends main 
    
    //Create new character method
    public static void createCharacter()
    {
        for (int repetitionCounter = 20; repetitionCounter > 0; repetitionCounter--)
        {
             printCreationPrompt(repetitionCounter);
             userSelection = input.nextInt();
             repetitionCounter += exectueStatChoice();
        }
       
    }
    
    //Character creation prompt
    public static void printCreationPrompt(int statPoints)
    //parameters is number of loops left
    {
    System.out.println("\nHealth:"+heroHealth+"|Attack Power:"+heroAttackPower+"|Magic Power:"+heroMagicPower+"\n1) +10 HP\n2) +1 Attack\n3) +3 MP");
    System.out.println("You have "+statPoints+" points left to spend.");
    }
    
    
    //hero's stats 
    public static int exectueStatChoice()
    {
        int pointsRefund = 0;
        if (userSelection == BUY_HP)
        {
            heroHealth = heroHealth + 10;
        }
        else if (userSelection == BUY_AP)
        {
            heroAttackPower = heroAttackPower + 1;
        }
        else if (userSelection == BUY_MP)
        {
            heroMagicPower = heroMagicPower+ 3;
        }
        else
        {
            System.out.println("Not a valid option.");
             pointsRefund++;
        }
        return pointsRefund;
      
    }
    
    
    //Menu prompt method
    public static void printMainMenuPrompt()
    {
        System.out.println("\n***It's dangerous out there, do you:***");
        System.out.println("1) Find adventure");
        System.out.println("2) Fight the dragon");
        System.out.println("3) Quit and go home");
        userSelection = input.nextInt();
    }
    
    
    //Adventure Choice method
     public static void exectueAdventureChoice()
    { 
        if (userSelection == ADVENTURE)
        {
        generateMonster();
        runCombatLoop();
        }
        
        else if (userSelection == DRAGON)
        {
        createDragon();
        runCombatLoop();
        }
        
        else if (userSelection == QUIT)
        {
            gameStatus = Status.QUIT;
        }
        
        else 
        {
            System.out.println("Error, please select valid option.");
        }
    } 
    
    
    public static void generateMonster()
    {
        int randomMonster = randomGenerator.nextInt(3);
        
        if (randomMonster == 0)
        {
            createGoblin();
        }
        else if (randomMonster == 1)
        {
            createOrk();
        }
        else if (randomMonster == 2)
        {
            createTroll();
        }
    }
    
    
    public static void createGoblin()
    {
        String monster = "goblin";
        monsterAttackPower = 8 + randomGenerator.nextInt(4);
        monsterHealth = 75 + randomGenerator.nextInt(24);
        monsterExperiencePoint = 1;
    }
    
     public static void createTroll()
    {
       String monster = "troll";
       monsterAttackPower = 15 + randomGenerator.nextInt(4);
       monsterHealth = 150 + randomGenerator.nextInt(49);
       monsterExperiencePoint = 5;
    }
    
     public static void createOrk()
    {
        String monster = "Ork";
       monsterAttackPower = 12 + randomGenerator.nextInt(4);
       monsterHealth = 100 + randomGenerator.nextInt(24);
       monsterExperiencePoint = 3;
    }
    
    
    public static void createDragon()
    {
       String monster = "dragon";
       monsterAttackPower = 50;
       monsterHealth = 1000;
       monsterExperiencePoint = 20;
    }
    
    
    public static void runCombatLoop()
    {
        isFighting = true;
        isMonsterAlive();
        isPlayerAlive();
        while (isFighting == true && isMonsterAlive() == true && isPlayerAlive() == true)
        {
        runCombatRound();
        }
    }
    
    public static boolean isMonsterAlive()
    {
        if (monsterHealth >0)
        {
            return true;
        }
        return false;
    }
    
    public static boolean isPlayerAlive()
    {
        if (heroHealth >0)
        {
            return true;
        }
        return false;
    }
    
    public static void runCombatRound()
    {
        printCombatPrompt();
        userSelection = input.nextInt();
        if (executeCombatChoice() == true)
        {
            monsterAttack();
            updateCombatResults();
            printResultMessage();
        }
    }
    public static void printCombatPrompt ()
    {
        System.out.println("printCombatPrompt invoked!");
    }
    
    public static boolean executeCombatChoice()
    {
        System.out.println("executeCombatChoice invoked!");
        return true;
    }
    
    public static void monsterAttack()
    {
        System.out.println("monsterAttack invoked!");
    } 
    
    public static void updateCombatResults()
    {
        System.out.println("updateCombatResults invoked!");
        isFighting = false;
    }
    
     public static void printResultMessage()
    {
        System.out.println("printResultMessage invoked!");
    }
    //Print end message method
    public static void printEndMessage()
    {
        System.out.println("\nYour level is "+heroLevel+".");
        if (gameStatus == Status.LOSE)
        {
            System.out.println("You have been defeated.");
        }
        else if (gameStatus == Status.WIN)
        {
            System.out.println("Congrats! You won!");
        }
        else if (gameStatus == Status.QUIT)
        {
            System.out.println("You quit.");
        }
    }
}