// Homework 1 section 3 for CSCI_1583 Lecture.  
//This program creates a text based combat game.
//Section 3 - Initialize input variables. Add scanner for player input. 

import java.util.Scanner; //imports scanner for interaction with user


public class CombatCalculator3

{
    public static void main(String[] args)
    
    {
        
    Scanner input = new Scanner(System.in); //inputs scanner into program
    
    //Monster data variables
    String monster = "goblin";//Declare variable for monster's name and initialize it to “goblin.”
    int monsterHealth = 100;//Declare variable for monster's health and initialize it 100
    int monsterAttackPower = 15;//Declare variable for monster's attack power and initialize it to 15


    //Hero data variables
    int heroHealth = 100; //Declare variable for Hero's health and initialize it to 100
    int heroAttackPower = 12; //Declare variable for Hero's attack power and initialize it to 12
    int heroMagicPower = 0; //Declare variable for Hero's magic power and initialize it to 0
    
    //Player variable from user
    int combatOption;
    
     //Reports combat status
    System.out.println("You are fighting a goblin!");
    System.out.println("The goblin's health is at: " + monsterHealth);
    System.out.println("Your health is at: " + heroHealth);
    System.out.println("Your MP status is: " + heroMagicPower);
    
    //Combat menu prompt
    System.out.format("%nCombat Options:");
    System.out.format("%nOption 1) Sword Attack");
    System.out.format("%nOption 2) Cast Spell");
    System.out.format("%nOption 3) Charge Mana");
    System.out.format("%nOption 4) Run Away%n");
    System.out.println("Which action would you like to perform? ");
    combatOption = input.nextInt();
    
    }
}