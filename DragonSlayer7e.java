/*
Homework #2
A game where the player creates a character and levels it up to kill a dragon.

Refinement #7e - Calculate damage, mana, and level up. 
*/

import java.util.Random;
import java.util.Scanner;
public class DragonSlayer7e

{
    private static Random randomGenerator = new Random();
    private enum Status {CONTINUE, WIN, LOSE, QUIT};
    public static Scanner input = new Scanner(System.in);
    private static Status gameStatus; 
    public static int userSelection;
    public static final int ADVENTURE = 1;
    public static final int DRAGON = 2;
    public static final int QUIT = 3;
    public static int heroLevel =1;
    private static int heroHealth = 0;
    private static int heroAttackPower = 0;
    private static int heroMagicPower = 0;
    private static final int BUY_HP = 1;
    private static final int BUY_AP = 2;
    private static final int BUY_MP = 3;
    public static final int GOBLIN = 0;
    public static final int ORK = 1;
    public static final int TROLL = 2;
    public static final int MELEE = 1;
    public static final int MAGIC = 2;
    public static final int CHARGE = 3;
    public static final int RUN = 4;
    public static String monster;
    public static int monsterHealth;
    public static int monsterAttackPower;
    public static int monsterExperiencePoint;
    public static boolean isFighting;
    public static void main(String[] arvs)
    
    { //starts main 
    
    System.out.println("Your village is being attacked by monsterous creatures. Kill a dragon to become a hero and win.");
    createCharacter();
    
        gameStatus = Status.CONTINUE;
        while (gameStatus == Status.CONTINUE)
        {
        printMainMenuPrompt();
        exectueAdventureChoice();
        }
        printEndMessage();
    }//ends main 
    
    //Create new character method
    public static void createCharacter()
    {
        for (int repetitionCounter = 20; repetitionCounter > 0; repetitionCounter--)
        {
             printCreationPrompt(repetitionCounter);
             userSelection = input.nextInt();
             repetitionCounter+= exectueStatChoice();
        }
       
    }
    
    //Character creation prompt
    public static void printCreationPrompt(int statPoints)
    //parameters is number of loops left
    {
    System.out.println("\nHealth:"+heroHealth+"|Attack Power:"+heroAttackPower+"|Magic Power:"+heroMagicPower+"\n1) +10 HP\n2) +1 Attack\n3) +3 MP");
    System.out.println("You have "+statPoints+" points left to spend.");
    }
    
    
    //hero's stats 
    public static int exectueStatChoice()
    {
        int pointsRefund = 0;
        if (userSelection == BUY_HP)
        {
            heroHealth = heroHealth + 10;
        }
        else if (userSelection == BUY_AP)
        {
            heroAttackPower = heroAttackPower + 1;
        }
        else if (userSelection == BUY_MP)
        {
            heroMagicPower = heroMagicPower + 3;
        }
        else
        {
            System.out.println("Not a valid option.");
             pointsRefund++;
        }
        return pointsRefund;
      
    }
    
    
    //Menu prompt method
    public static void printMainMenuPrompt()
    {
        System.out.println("\n***It's dangerous out there, do you:***");
        System.out.println("1)Find adventure\n2)Fight the dragon\n3)Quit and go home");
        userSelection = input.nextInt();
    }
    
    
    //Adventure Choice method
     public static void exectueAdventureChoice()
    { 
        if (userSelection == ADVENTURE)
        {
        generateMonster();
        runCombatLoop();
        }
        
        else if (userSelection == DRAGON)
        {
        createDragon();
        runCombatLoop();
        }
        
        else if (userSelection == QUIT)
        {
            gameStatus = Status.QUIT;
        }
        
        else 
        {
            System.out.println("Error, please select valid option.");
        }
    } 
    
    
    public static void generateMonster()
    {
        int randomMonster = randomGenerator.nextInt(3);
        
        if (randomMonster == 0)
        {
            createGoblin();
        }
        else if (randomMonster == 1)
        {
            createOrk();
        }
        else if (randomMonster == 2)
        {
            createTroll();
        }
    }
    
    
    public static void createGoblin()
    {
        monster = "goblin";
        monsterAttackPower = 8 + randomGenerator.nextInt(4);
        monsterHealth = 75 + randomGenerator.nextInt(24);
        monsterExperiencePoint = 1;
    }
    
     public static void createTroll()
    {
       monster = "troll";
       monsterAttackPower = 15 + randomGenerator.nextInt(4);
       monsterHealth = 150 + randomGenerator.nextInt(49);
       monsterExperiencePoint = 5;
    }
    
     public static void createOrk()
    {
       monster = "ork";
       monsterAttackPower = 12 + randomGenerator.nextInt(4);
       monsterHealth = 100 + randomGenerator.nextInt(24);
       monsterExperiencePoint = 3;
    }
    
    
    public static void createDragon()
    {
       monster = "dragon";
       monsterAttackPower = 50;
       monsterHealth = 1000;
       monsterExperiencePoint = 20;
    }
    
    
    public static void runCombatLoop()
    {
        isFighting = true;
        while (isPlayerAlive() && isMonsterAlive() && isFighting )
        {
        runCombatRound();
        }
    }
    
    public static boolean isMonsterAlive()
    {
        if (monsterHealth > 0)
        {
            return true;
        }
        return false;
    }
    
    public static boolean isPlayerAlive()
    {
        if (heroHealth > 0)
        {
            return true;
        }
        return false;
    }
    
    public static void runCombatRound()
    {
        printCombatPrompt();
        int userSelection = input.nextInt();
        if (executeCombatChoice(userSelection) == true)
        {
            monsterAttack();
            updateCombatResults();
            printResultMessage();
        }
    }
    public static void printCombatPrompt()
    {
        System.out.println("\nYou are fighting a monstrous "+monster+"!");
        System.out.println("Monster's health is: "+monsterHealth);
        System.out.println("Your HP: "+heroHealth);
        System.out.println("Your MP: "+heroMagicPower);
        System.out.println("\n1)Sword Attack\n2)Cast Spell\n3)Charge Mana\n4)Run Away");
    }
    
    public static boolean executeCombatChoice(int userSelection)
    {
        boolean validChoice = true;
        String description = "";
        
        if (userSelection == MELEE)
        {
            description = meleeOption();
        }
        else if (userSelection == MAGIC)
        {
            validChoice = hasEnoughMana();
            description = magicOption();
            
        }
        else if (userSelection == CHARGE)
        {
            description = chargeOption();
           
        }
        else if (userSelection == RUN)
        {
           description = fleeOption();
            
        }
        else 
        {
            description = "I don't understand that command.";
            validChoice = false;
        }
        System.out.println(description);
        return validChoice;
    }
    
    public static String meleeOption()
    
    {
        int melee; 
        melee = calculateMeleeDamage(heroAttackPower);
        monsterHealth -= melee;
        return "You strike the "+monster+" with a sword for "+melee+" damage.";
    }
    
    public static int calculateMeleeDamage(int heroAttack)
    {
        int randomAttack;
        randomAttack = randomGenerator.nextInt(heroAttack);
        return randomAttack;
    } 
    
    public static boolean hasEnoughMana()
    {
        if (heroMagicPower >= 3)
        {
            return true;
        }
        return false;
    }
    
    public static String magicOption()
    {
        String magicText = "";
        int magic;
        magic = calculateMagicDamage(monsterHealth, heroMagicPower);
        monsterHealth -= magic;
        magicText = getMagicText();
        calculateManaLeft();
        return magicText;
    }
    
    public static int calculateMagicDamage(int defenderHP, int attackersMana)
    {
        int damage = 0;
        if (attackersMana >= 3)
        {
            damage = defenderHP/2;
        }
        return damage;
    }
    
    public static String getMagicText()
    {
        String magicText = "";
        if (hasEnoughMana() == true)
        {
            magicText = "You cast the weaken spell on the "+monster+".";
        }
        else if (hasEnoughMana() == false)
        {
            magicText = "You don't have enough mana.";
        }
        return magicText;
    }
    
    public static void calculateManaLeft()
    {
        if (hasEnoughMana() == true)
        {
            heroMagicPower = heroMagicPower-3;
        }
    }
    
    public static String chargeOption()
    {
        heroMagicPower++;
        return "You charge and focus your magic power.";
    }
    
    public static String fleeOption()
    {
        isFighting = false;
        return "You run away!";
    }
    
    public static void monsterAttack()
    {
        int damage;
        if (isMonsterAlive() == true && isFighting == true)
        {
          damage = calculateMeleeDamage(monsterAttackPower);
          heroHealth -= damage;
          System.out.println("The "+monster+" attacks you for "+damage+" damage.");
        }
    } 
    
    public static void updateCombatResults()
    {
        if (isMonsterAlive() == false)
        {
            levelUp();
        }
        else if (isPlayerAlive() == false)
        {
           gameStatus = Status.LOSE;
        }
    }
    public static void levelUp()
    {
       heroLevel += monsterExperiencePoint;
       heroAttackPower += monsterExperiencePoint;
       heroHealth = heroHealth + 50 + randomGenerator.nextInt(30);
       System.out.println("You have leveled up!\nYour current level is "+heroLevel+".");
       if (monster == "dragon")
       {
           gameStatus = Status.WIN;
       }
    }
    
    
     public static void printResultMessage()
    {
        if (isMonsterAlive() == false)
        {
            System.out.println("You defeated the monstrous "+monster+"!!!");
        }
        else if (isPlayerAlive() == false)
        {
            System.out.println("You were defeated by the monstrous "+monster+".");
        }
    }
    //Print end message method
    public static void printEndMessage()
    {
        if (gameStatus == Status.LOSE)
        {
            System.out.println("\nYou got to level "+heroLevel+".\n... and then you died.");
        }
        else if (gameStatus == Status.WIN)
        {
            System.out.println("Congrats! You won!");
        }
        else if (gameStatus == Status.QUIT)
        {
            System.out.println("You quit.");
        }
    }
}