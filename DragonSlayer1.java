/*
Homework #2
A game where the player creates a character and levels it up to kill a dragon.

Refinement #1 - Provide the base outline for the game’s algorithm.
*/


import java.util.Scanner;
public class DragonSlayer1

{
   
    private enum Status {CONTINUE, WIN, LOSE, QUIT};
    public static Scanner input = new Scanner(System.in);
    private static Status gameStatus;
    public static int userSelection;
    public static void main(String[] arvs)
    
    
    
    { //starts main 
    
    System.out.println("Your village is being attacked by monsterous creatures. Kill a dragon to become a hero and win.");
    createCharacter();
    
    
    gameStatus = Status.CONTINUE;
    
    {
        printMainMenuPrompt();
        //System.out.println("What would you like to do?");
        userSelection = input.nextInt();
        exectueAdventureChoice();

    }
        printEndMessage();
        
    }//ends main 
    
    //Create new character method
    public static void createCharacter()
    {
        System.out.println("createCharacter invoked!");
        
    }
    //Menu prompt method
    public static void printMainMenuPrompt()
    
    {
        System.out.println("printMainMenuPrompt invoked!");
    }
    //Adventure Choice method
     public static void exectueAdventureChoice()
    
    {
        System.out.println("exectueAdventureChoice invoked!");
    } 
    //Print end message method
    public static void printEndMessage()
    {
        System.out.println("printEndMessage invoked!");
    }
}