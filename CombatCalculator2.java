// Homework 1 section 2 for CSCI_1583 Lecture.  
//This program creates a text based combat game.
//Section 2 - Initialize Output Variables.

public class CombatCalculator2

{
    public static void main(String[] args)
    
    {
        /*Monster data variables*/
    String monster = "goblin";//Declare variable for monster's name and initialize it to “goblin.”
    int monsterHealth = 100;//Declare variable for monster's health and initialize it 100
    int monsterAttackPower = 15;//Declare variable for monster's attack power and initialize it to 15

    /*Hero data variables*/
    int heroHealth = 100; //Declare variable for Hero's health and initialize it to 100
    int heroAttackPower = 12; //Declare variable for Hero's attack power and initialize it to 12
    int heroMagicPower = 0; //Declare variable for Hero's magic power and initialize it to 0
    
      //reports combat status
    System.out.println("You are fighting a goblin!");
    System.out.println("Goblin's health is at:  " + monsterHealth);
    System.out.println("Your health is currently at: " + heroHealth);
    System.out.println("Your MP status is" + heroMagicPower);
    }
}