/*
Homework #2
A game where the player creates a character and levels it up to kill a dragon.

Refinement #2c - Implement tasks.
*/


import java.util.Scanner;
public class DragonSlayer2c

{
   
    private enum Status {CONTINUE, WIN, LOSE, QUIT};
    public static Scanner input = new Scanner(System.in);
    private static Status gameStatus;
    public static int userSelection;
    public static final int ADVENTURE = 1;
    public static final int DRAGON = 2;
    public static final int QUIT = 3;
    public static void main(String[] arvs)
    
    
    
    { //starts main 
    
    System.out.println("Your village is being attacked by monsterous creatures. Kill a dragon to become a hero and win.");
    createCharacter();
    
    
        gameStatus = Status.CONTINUE;
        printMainMenuPrompt();
        exectueAdventureChoice();
     
        
    }//ends main 
    
    //Create new character method
    public static void createCharacter()
    {
        for (int repetitionCounter = 0; repetitionCounter <20; ++repetitionCounter)
        {
             printCreationPrompt();
             userSelection = input.nextInt();
             exectueStatChoice();
        }
       
    }
    
    //Character creation prompt
   public static void printCreationPrompt()
    //parameters is number of loops left
    {
    System.out.println("printCreationPrompt invoked!");
    }
    //hero's stats 
    public static int exectueStatChoice()
    
    {
        System.out.println("exectueStatChoice invoked!");
        return 0;
    }
    
    
    //Menu prompt method
    public static void printMainMenuPrompt()
    {
        System.out.println("***It's dangerous out there, do you:***");
        System.out.println("1) Find adventure");
        System.out.println("2) Fight the dragon");
        System.out.println("3) Quit and go home");
        userSelection = input.nextInt();
    }
    //Adventure Choice method
     public static void exectueAdventureChoice()
    
    {
        if (userSelection == ADVENTURE)
        {
        generateMonster();
        runCombatLoop();
        }
        
        else if (userSelection == DRAGON)
        {
        createDragon();
        runCombatLoop();
        }
        
        else if (userSelection == QUIT)
        {
            printEndMessage();
        }
        
        else 
        {
            System.out.println("Error, please select valid option.");
        }
    } 
    
    public static void generateMonster()
    {
        System.out.println("generateMonster invoked!");
    }
    
    public static void createDragon()
    {
        System.out.println("createDragon invoked!");
    }
    
    public static void runCombatLoop()
    
    {
        System.out.println("runCombatLoop invoked!");
    }
    
    //Print end message method
    public static void printEndMessage()
    {
        System.out.println("printEndMessage invoked!");
    }
}